package com.sortingalgorithm.test;

import com.sortingalgorithm.insertionsort.InsertionSort;

/**
 * The best case input is an array that is already sorted.
 * In this case insertion sort has a linear running time (i.e., Θ(n)).
 * During each iteration, the first remaining element of the input is only compared with
 * the right-most element of the sorted subsection of the array.
 * The simplest worst case input is an array sorted in reverse order.
 * The set of all worst case inputs consists of all arrays
 * where each element is the smallest or second-smallest of the elements before it.
 * In these cases every iteration of the inner
 * loop will scan and shift the entire sorted subsection of the array before
 * inserting the next element. This gives insertion sort a quadratic running time (i.e., O(n2)).
 * The average case is also quadratic, which makes insertion sort impractical for sorting large arrays.
 * However, insertion sort is one of the fastest algorithms for sorting very small arrays, even faster than quicksort; indeed,
 * good quicksort implementations use insertion sort for arrays smaller than a certain threshold, also when arising as subproblems;
 * the exact threshold must be determined experimentally and depends on the machine, but is commonly around ten.


 */
public class InsertionSortTest {

    public static void main(String[] args) {
        int[] array = {3, 1, 4, 5, 2, -20, 0, -4, 9, 2};

        int[] array2 = InsertionSort.insertionSort(array);

        for (int i = 0; i < array2.length; i++) {
            System.out.println("array[" + i + "] = " + array[i]);
        }
    }
}
