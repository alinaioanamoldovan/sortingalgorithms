package com.sortingalgorithms.mergesorttest;

import com.sortingalgorithms.mergesort.MergeSort;

/**
 * Steps to implement Merge Sort:

 1) Divide the unsorted array into n partitions, each partition contains 1 element. Here the one element is considered as sorted.
 2) Repeatedly merge partitioned units to produce new sublists until there is only 1 sublist remaining. This will be the sorted list at the en
 */

/**
 * Merge sort is a fast, stable sorting routine with guaranteed O(n*log(n)) efficiency.
 * When sorting arrays, merge sort requires additional scratch space proportional to the size of the input array.
 * Merge sort is relatively simple to code and offers performance typically only slightly below that of quicksort.

 */
public class MergeSortTest {

    public static void main(String[] args){
        int[] array = {40,20,10,4,0,566,23,6,8,-1,-30};
        int[] tempArray = new int[array.length];

        MergeSort.mergeSort(array,tempArray);

        for(int i:array){
            System.out.println(i);
        }
    }
}
