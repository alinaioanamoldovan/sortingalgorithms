package com.sortingalgorithms.mergesort;

public class MergeSort {

    private static void mergeParts(int lowerIndex, int middle, int higherIndex, int[] array, int[] tempArray) {
        tempArray = array.clone();

        int i = lowerIndex;
        int j = middle + 1;
        int k = lowerIndex;

        while (i <= middle && j <= higherIndex) {
            if (tempArray[i] <= tempArray[j]) {
                array[k] = tempArray[i];
                i++;
            } else {
                array[k] = tempArray[j];
                j++;
            }
            k++;
        }

        while (i <= middle) {
            array[k] = tempArray[i];
            k++;
            i++;
        }
    }

    private static void doMergeSort(int lowerIndex,int higherindex,int[] array,int[] tempArray){

        if(lowerIndex<higherindex){
            int middle = lowerIndex + (higherindex-lowerIndex)/2;
            //Below step sorts the left side of the array
            doMergeSort(lowerIndex,middle,array,tempArray);
            //Bellow step sorts the right side of the array
            doMergeSort(middle+1,higherindex,array,tempArray);
            //now merge both sides
            mergeParts(lowerIndex,middle,higherindex,array,tempArray);
        }

    }

    public static void mergeSort(int[] array,int[] tempArray){
        doMergeSort(0,array.length - 1,array,tempArray);
    }
}
