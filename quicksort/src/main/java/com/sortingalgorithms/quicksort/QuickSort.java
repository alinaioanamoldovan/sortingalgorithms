package com.sortingalgorithms.quicksort;

public class QuickSort {

    public static void sort(int[] array){
        int length = array.length;
        quicksort(0,length-1,array);
    }

    private static void quicksort(int lowerindex,int higherindex,int[] arr){
        int i = lowerindex;
        int j = higherindex;

        //calculate pivot, taking it as the middle of the array
        int pivot = arr[lowerindex+(higherindex-lowerindex)/2];

        //divide into two arrays

        while(i<=j){
            /**
             * In each iteration, we will identify a number from left side which
             * is greater then the pivot value, and also we will identify a number
             * from right side which is less then the pivot value. Once the search
             * is done, then we exchange both numbers.
             */
            while (arr[i] < pivot){
                i++;
            }

            while (arr[j] > pivot){
                j--;
            }

            if(i<=j){
                exchangeNumbers(i,j,arr);
                //move the index to next position on both sides
                i++;
                j--;
            }
        }
        if (lowerindex < j)
            quicksort(lowerindex, j,arr);
        if (i < higherindex)
            quicksort(i, higherindex,arr);

    }

    private static void exchangeNumbers(int i, int j,int[] array) {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}
