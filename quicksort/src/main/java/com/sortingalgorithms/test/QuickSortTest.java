package com.sortingalgorithms.test;

import com.sortingalgorithms.quicksort.QuickSort;

public class QuickSortTest {

    public static void main(String[] args){
        int[] array = {1,4,0,-5,2,4,-20,-2};

        QuickSort.sort(array);

        for(int i=0;i<array.length;i++){
            System.out.println(array[i]);
        }
    }
}
