package com.sortingalgorithms.test;

import com.sortingalgorithms.selectionsort.SelectionSort;

public class SelectionSortTest {

    public static void main(String[] args){
        int[] array = {100,3,-4,30,5,7,1,0,-11,-23};

        int[] arr2 = SelectionSort.selectionSort(array);

        for(int i:arr2){
            System.out.print(i);
            System.out.print(", ");
        }
    }
}
